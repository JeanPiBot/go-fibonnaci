package main

import "fmt"

func Fibonnaci(n int) []int {
	// this array is for save the result
	var result []int
	// These numbers are to start it
	num1 := 0
	num2 := 1

	for i := 2; i <= n; i++ {
		var num3 = num1 + num2
		result = append(result, num3)
		num1 = num2
		num2 = num3
	}

	return result
}

func main() {
	fmt.Println(Fibonnaci(15))
}
